<?php

declare(strict_types=1);

namespace Api;

/**
 * The configuration provider for the Api module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'invokables' => [
                Api\Handler\DoctorQuotesHandler::class => Api\Handler\DoctorQuotesHandler::class,
                Api\Model\Quote::class => Api\Model\Quote::class,
            ],
            'factories'  => [
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates() : array
    {
        return [
            'paths' => [
                'api'    => [__DIR__ . '/../templates/'],
            ],
        ];
    }
}
