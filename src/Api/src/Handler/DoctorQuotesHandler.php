<?php

namespace Api\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Api\Model\Quote;

class DoctorQuotesHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        // PHP 7+: Null coalescing operator (??)
     $dr_num = $request->getQueryParams()['dr_num'] ?? null;
//      $dr_num = $request->getAttribute('dr_num');

        $dq = include('data/doctor_quotes.php');
        $q = new Quote($dq);

        $quote = $dr_num ?
            sprintf('%s — %s', $q->getQuotes($dr_num)[$qoute_number], $q->getSourceName($dr_num)):
            $quote = $q->getRandomQuote();

        return new JsonResponse(['dr_says' => $quote]);
    }
}
