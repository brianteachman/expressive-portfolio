<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;

use Api\Model\Quote;

class HomePageHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        string $containerName,
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->containerName = $containerName;
        $this->router        = $router;
        $this->template      = $template;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $dq = new Quote(include('data/doctor_quotes.php'));
        // $profile_data = include('data/profile_data.php');
        $data = [
            'dr_quote' => $dq->getRandomQuote()
        ];

        return new HtmlResponse($this->template->render('app::home-page', $data));
    }

    /**
     * This should get moved into a js function
     *
     * @param ServerRequestInterface $request
     * @return string[] 
     */
    private function fetchDrQuote(ServerRequestInterface $request)
    {
        // use api
        $jsonurl = "http://" . $request->getUri()->getHost() . "/api/v1/doctorquotes";
        $json = file_get_contents($jsonurl);
        $data = json_decode($json, true);
        return $data["dr_quote"];
    }
}
