<?php

declare(strict_types=1);

namespace Portfolio\Handler;

use Psr\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class PortfolioHandlerFactory
{
    public function __invoke(ContainerInterface $container) : PortfolioHandler
    {
        return new PortfolioHandler($container->get(TemplateRendererInterface::class));
    }
}
