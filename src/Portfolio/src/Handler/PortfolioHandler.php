<?php

declare(strict_types=1);

namespace Portfolio\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class PortfolioHandler implements RequestHandlerInterface
{
    /**
     * @var TemplateRendererInterface
     */
    private $renderer;

    public function __construct(TemplateRendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $dq = new \Portfolio\Model\Portfolio( include('data/portfolio_data.php') );
        $data = $dq->getAllProjects();

        // Render and return an HTML response:
        return new HtmlResponse($this->renderer->render(
            // the template to render
            'portfolio::index',
            [   // Array of parameters to pass to template
                'software' => $data["software"],
                'electronics' => $data["electronics"],
            ]
        ));
    }
}
