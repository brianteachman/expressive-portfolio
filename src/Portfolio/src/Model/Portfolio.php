<?php
/**
 * Author: Brian Teachman
 * Date: 9/20/2019
 */

namespace Portfolio\Model;

class Portfolio
{
    /**
     * $portfolio_data = [
     *     'software' => [],
     *     'electronics' => [],
     *     'project management' => [],
     * ]
     */
    public $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getAllProjects(): array
    {
        return $this->data;
    }

    public function getProjectsByType(string $project_type): array
    {
        return $this->data[$project_type];
    }

    public function getProjectCount(): int
    {
        $total = 0;
        foreach ($this->data as $items) {
            foreach ($items as $items) {
                $total += 1;
            }
        }
        return $total;
    }
}
