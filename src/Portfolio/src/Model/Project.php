<?php
/**
 * Author: Brian Teachman
 * Date: 9/20/2019
 */

namespace Portfolio\Model;

class Project
{
    // @var string  (school, personal, contract)
    public $type;

    // @var string
    public $title;

    // @var string
    public $description;

    // @var string  @TODO: Use DateTime class or something.
    public $date;

    // @var array of tags (php, css, arduino, etc.)
    public $tags;

    /**
     * $data = [
     *      'type' => "",
     *      'title' => "",
     *      'description' => "",
     *      'date' => "",
     *      'tags' => [
     *          "php", "css", "etc.",
     *      ],
     * ]
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->type = $data['type'];
        $this->title = $data['title'];
        $this->description = $data['description'];
        $this->date = $data['date'];
        $this->tags = $data['tags'];
    }
}