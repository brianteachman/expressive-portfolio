<?php
/**
 * User: mrtea
 * Date: 9/13/2017
 * License:
 */
/*
 * References:
 * 1. https://en.wikiquote.org/wiki/The_Doctor
 * 2. http://www.greatlifequotes.org/2011/07/doctor-who-quotes/
 */

$doctor_quotes = [
	[
		'name' => "The War Doctor",
		'actor' => "",
		'quotes' => [
			"Great men are forged in fire. It is the privilege of lesser men to light the flame.",
			"Why is there never a big red button?",
			"Never give up. Never give in.",
			"What I did, I did without choice, in the name of peace and sanity."
		]
	],
	[
		'name' => "The First Doctor",
		'actor' => "",
		'quotes' => [
			"As we learn about each other, so we learn about ourselves.",
			"I don't believe that man was made to be controlled by machines. Machines can make laws, but they can not preserve justice. Only human beings can do that.",
			"Our lives are important — at least to us — and as we see, so we learn… Our destiny is in the stars, so let’s go and search for it.",
			"It all started out as a mild curiosity in the junkyard, and now it's turned out to be quite a great spirit of adventure."
		]
	],
	[
		'name' => "The Second Doctor",
		'actor' => "",
		'quotes' => [
			"Logic merely enables one to be wrong with authority.",
			"People spend all their time making nice things and then other people come along and break them.",
			"Give a monkey control of its environment, and it will fill the world with bananas.",
			"There are some corners of the universe which have bred the most terrible things. Things that act against everything we believe in. They must be fought!",
			"Slower! SLOWER! Concentrate on one thing! ONE thing!",
			"Life depends on change, and renewal.",
			"We've done exactly as you calculated, haven't we?",
			"An unintelligent enemy is far less dangerous than an intelligent one, Jamie. Just act stupid. Do you think you can manage that?",
			"I hate computers and refuse to be bullied by them!",
			"Keep it confused, feed it with useless information — I wonder if I have a television set handy?"
		]
	],
	[
		'name' => "The Third Doctor",
		'actor' => "",
		'quotes' => [
			"I reversed the polarity of the neutron flow.",
			"A straight line may be the shortest distance between two points, but it is by no means the most interesting.",
			"Allow me to congratulate you, sir. You have the most totally closed mind that I’ve ever encountered.",
			"It's all quite simple — I am he and he is me!"
		]
	],
	[
		'name' => "The Fourth Doctor",
		'actor' => "",
		'quotes' => [
			"There’s no point being grown-up if you can’t be childish sometimes.",
			"That’s monstrous! Vaporisation without representation is against the constitution.",
			"First things first, but not necessarily in that order.",
			'Never cared much for the word "impregnable." Sounds a bit too much like "unsinkable."',
			"I'm a Time Lord...I'm not a human being; I walk in eternity...",
			"Something's going on contrary to the laws of the universe. I must find out what!",
			"Deactivating a generator loop without the correct key is like repairing a watch with a hammer and chisel. One false move and you'll never know the time again.",
			"Answers are easy. It's asking the right questions which is hard.",
			"The very powerful and the very stupid have one thing in common. Instead of altering their views to fit the facts, they alter the facts to fit their views...which can be very uncomfortable if you happen to be one of the facts that needs altering.",
			"To the rational mind, nothing is inexplicable; only unexplained.",
			"That's trans-dimensional engineering; a key Time Lord discovery!",
			"I'm a very dangerous fellow when I don't know what I'm doing.",
			"What can't be cured must be endured. … Oh don't listen to me. I never do.",
			"It's the end. But the moment has been prepared for."
		]
	],
	[
		'name' => "The Fifth Doctor",
		'actor' => "",
		'quotes' => [
			"Yes. The second law of thermodynamics is taking its toll on the old thing. Entropy increases.",
			"Yes. Still, the future lies this way.",
			"Well, there’s a probability of anything. Statistically speaking, if you gave typewriters to a treeful of monkeys, they’d eventually produce the works of William Shakespeare.",
			"There’s always something to look at if you open your eyes.",
			"Why are Earth people so parochial?",
			"The illusion is always one of normality.",
			"You know how it is; you put things off for a day and next thing you know, it's a hundred years later.",
			"For some people, small, beautiful events is what life is all about!"
		]
	],
	[
		'name' => "The Sixth Doctor",
		'actor' => "",
		'quotes' => [
			"What's the use of a good quotation if you can't change it?",
			"Planets come and go. Stars perish. Matter disperses, coalesces, forms into other patterns, other worlds. Nothing can be eternal. "
		]
	],
	[
		'name' => "The Seventh Doctor",
		'actor' => "",
		'quotes' => [
			"Are these antiques dotted about all over the building? It really is a splendid piece of audioarchitectonicalmetrasynchosity!",
			"If we fight like animals, we die like animals.",
			"Anybody remotely interesting is mad in some way.",
			"Among all the varied wonders of the universe, there's nothing so firmly clamped shut as the military mind.",
			"All is change, all is movement",
			"I am not like you."
		]
	],
	[
		'name' => "The Eighth Doctor",
		'actor' => "",
		'quotes' => [
			"I love humans. Always seeing patterns in things that aren’t there."
		]
	],
	[
		'name' => "The Ninth Doctor",
		'actor' => "",
		'quotes' => [
			"Could you hurry up and close the door? Your ship’s about to blow up; there’s gonna be a draft.",
			"I love a happy medium!",
		]
	],
	[
		'name' => "The Tenth Doctor",
		'actor' => "",
		'quotes' => [
			"Allons-y!",
			"Think you've seen it all? Think again. Outside those doors, we might see anything. We could find new worlds, terrifying monsters, impossible things. And if you come with me... nothing will ever be the same again!",
			"Never say never ever.",
			"Some people live more in twenty years than others do in eighty. It's not the time that matters, it's the person.",
			"There's no such thing as an ordinary human.",
			"We're at the end of the universe. Okay?! Right at the edge of knowledge itself! And you're busy... blogging!",
			"There are laws. There are laws of time. Once upon a time there were people in charge of those laws, but they died. They all died."
		]
	],
	[
		'name' => "The Eleventh Doctor",
		'actor' => "",
		'quotes' => [
			"There are laws. There are laws of time. Once upon a time there were people in charge of those laws, but they died. They all died.",
			"You’re bluffing. There isn’t a sincere bone in your body… there isn’t a bone in your body!",
			"Come on, Rory! It isn't rocket science, it's just quantum physics!",
			"Never use force, you just embarrass yourself. Unless you're cross, in which case... always use force!",
			"Never run when you're scared. Rule 7.",
			"Never knowingly be serious. Rule 27.",
			"You should always waste time when you don't have any. Time is not the boss of you. Rule 408.",
			"I am and always will be the optimist, the hoper of far-flung hopes and the dreamer of improblable dreams.",
			"*Throwing bread out of door* AND STAY OUT!",
			"I'd forgotten not all victories are about saving the universe.",
			"I have a thing. It's like a plan, but with more greatness.",
			"Annihilate? No. No violence, do you understand me? Not while I'm around. Not today, not ever. I'm the Doctor, the Oncoming Storm.",
			"I've never met anyone who wasn't important before?",
		]
	],
	[
		'name' => "The Twelveth Doctor",
		'actor' => "",
		'quotes' => [
			"Kidneys! I’ve got new kidneys! I don’t like the colour.",
			"The deep and lovely dark. We'd never see the stars without it.",
			"Pain is a gift. Without the capacity for pain, we can't feel the hurt we inflict.",
			"Never trust a hug. It's just a way to hide your face.",
			"Winning is all about looking happier than the other guy."
		]
	]
];

return $doctor_quotes;
